
/****************************************************************************************************** 
  	TEAM-C:			
        -------
	
  	Authors			
        -------
	I.TATAJI	10347	tatajiirrinki@gmail.com		Cell : 9704147867
	k.NAVEENA	10357	naveenakolanupaka10@gmail.com	Cell : 8374415216
	N.V.V.S NAIDU	10360	naidu.nama@gmail.com		Cell : 7729955884

        Purpose:
        --------

        --To manipulate the bits in an integer
        --here we are taking the run time inputs for data and bit position

******************************************************************************************************/
	

#include<iostream>
#include<cstdlib>
using namespace std;

//BitManipulator class to manipulate the data according to the given bit position
class BitManipulator {

  //data members of the BitManipulator class
  private:
	int data;
	int bit_position;
  //member functions of the BitManipulator class
  public:
  	//constructor of BitManipulator class
	BitManipulator(int data,int bit_position) {
		this->data=data;
		this->bit_position=bit_position;
	}
	//member function set-bit to set the given bit position
	void setBit() {
 		data|=(1<<bit_position);
	}

  	//member function clear-bit to clear the given bit position
	void clearBit() {
		data&=~(1<<bit_position);
	}
	
  	//member function complement-bit to complement the given bit position	
	void complementBit() {
		data^=(1<<bit_position);
	}

  	//member function display to display the data
	void display() {
		cout<<data<<endl;
		cout<<"in binary:";
		for(int i=31;i>=0;i--) {
			cout<<((data>>i)&1)<<" ";
		}
		cout<<endl;
	}

  	//member function menu to navigate between the all other member functions
	void menu() {

		int choice;
		cout<<endl<<"*******MENU********"<<endl;
		cout<<"1.set bit"<<endl;
		cout<<"2.clear bit"<<endl;
		cout<<"3.complement bit"<<endl;
		cout<<"enter your choice:"<<endl;
		cin>>choice;
		switch(choice) {
			case 1:
				setBit();
				break;
			case 2:	
				clearBit();
				break;	
			case 3:	
				complementBit();
				break;			
		}
	
	}
};

//main method 
int main(void) {

	system("clear");
	cout<<"**********BIT MANIPULATION PROGRAM**************"<<endl;
	int data,bit_position,choice;
	
	//reading the user input
	cout<<"enter the data to perform operations "<<endl;
	cin>>data;

	cout<<"enter the bit position:"<<endl;
	cin>>bit_position;

	//re-reading the bit position if it is invalid 
	while(bit_position<0 || bit_position>31) {
		cout<<"invalid bit position.Re-enter the bit position(0-31):"<<endl;
		cin>>bit_position;
	}

	//creating the instance for BitManipulator class
	BitManipulator bitmanipulator(data,bit_position);
	
	//calling the display member function before doing the operation
	cout<<"entered data before operation: ";
	bitmanipulator.display();

	//calling the menu member function to navigate
	bitmanipulator.menu();

	//calling the display member function after doing the operation
 	cout<<"data after operation : ";
	bitmanipulator.display();
	
	return 0;
}

//---------------------------
// OutPut
//---------------------------
**************BIT MANIPULATION PROGRAM**************
enter the data to perform operations 
12
enter the bit position:
5
entered data before operation: 12
in binary:0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 1 0 0 

*******MENU********
1.set bit
2.clear bit
3.complement bit
enter your choice:
1
data after operation : 44
in binary:0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 1 1 0 0 


/****************************************************************************************************** 
  	TEAM-C:			
        -------
	
  	Authors			
        -------
	I.TATAJI	10347	tatajiirrinki@gmail.com		Cell : 9704147867
	k.NAVEENA	10357	naveenakolanupaka10@gmail.com	Cell : 8374415216
	N.V.V.S NAIDU	10360	naidu.nama@gmail.com		Cell : 7729955884

        Purpose:
        --------

        --To find the number of equi-lateral triangles present in the given matrix
        --here we are generating the matrix randomly and calculating the all possible hops for that matrix

        Approach: This program uses the Standard Template Library(STL) of compiler flag -std=c++11
        --------

******************************************************************************************************/
	


#include<cstdlib>
#include<unistd.h>
#include<iostream>
#include<vector>

using namespace std;

//creating the TrianglesInMatrix class
class TrianglesInMatrix {

	//declaring the data members as private
	private:
		int rows_of_matrix,columns_of_matrix;
		vector<vector<int> > matrix;
	
	//defining the member functions as public
	public:
		//default constrctor for TrianglesInMatrix class to initialize the data members
		TrianglesInMatrix() {

			srand(getpid());
			rows_of_matrix=random()%10;
			columns_of_matrix=random()%10;

			for(int i = 0; i < rows_of_matrix; ++i) {
			matrix.push_back(vector<int>());
				for(int j=0;j<columns_of_matrix;j++)
					matrix[i].push_back(random()%10);
			}
		}

		//member function printMatrix to display the matrix
		void printMatrix() {
			for(int i=0;i<rows_of_matrix;i++,cout<<endl) {
				for(int j=0;j<columns_of_matrix;j++) {
					cout<<matrix[i][j]<<" ";
				}
			}
		}

		//member function findTriangles to find out the equi-lateral triangles present in given matrix
		void findTriangles() {
			int total_count=0;
			for(int no_of_hops=1;no_of_hops<=rows_of_matrix-1 && no_of_hops<=columns_of_matrix/2;no_of_hops++) {
				int count_for_hops=0;
				for(int i=0;i<rows_of_matrix-no_of_hops;i++) {
					for(int j=no_of_hops;j<columns_of_matrix-no_of_hops;j++) {

						if(matrix[i][j]>0) {

							if(matrix[i+no_of_hops][j-no_of_hops]>0 && matrix[i+no_of_hops][j+no_of_hops]>0) {
								count_for_hops++;
							}
						}
					}
				}
				cout<<"no.of "<<no_of_hops<<"-hop triangles:"<<count_for_hops<<endl;
				total_count+=count_for_hops;
			}
			cout<<"no.of total triangles:"<<total_count<<endl;
		}

};

//main method
int main(void) {

	//creating the  instance for TrianglesInMatrix class
	TrianglesInMatrix trianglesinmatrix;
	
	//calling the printMatrix method
	trianglesinmatrix.printMatrix();

	//calling the findTriangles method
	trianglesinmatrix.findTriangles();

	return 0;
}

//----------------------------------------
// OutPut
7 0 1 1 1 6 0 0 
9 5 9 7 2 4 3 9 
2 0 4 0 8 1 0 3 
2 0 4 6 4 2 4 1 
4 8 4 5 6 6 5 6 
4 6 5 6 0 0 7 3 
no.of 1-hop triangles:16
no.of 2-hop triangles:9
no.of 3-hop triangles:4
no.of 4-hop triangles:0
no.of total triangles:29
//----------------------------------------

